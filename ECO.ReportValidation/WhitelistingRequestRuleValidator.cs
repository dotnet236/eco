﻿using System.Linq;
using ECO.Entities;

namespace ECO.ReportValidation
{
    public class WhitelistingRequestRuleValidator : IReportRuleValidator
    {
        public EmailValidationResult Validate(EmailReport report, ReportRule rule)
        {
            return DictionaryValidator.Validate(report, rule);
        }
    }
}

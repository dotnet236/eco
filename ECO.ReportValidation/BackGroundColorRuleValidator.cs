﻿using System;
using System.Linq;
using ECO.Entities;
using HtmlAgilityPack;
using System.Text.RegularExpressions;

namespace ECO.ReportValidation
{
    public class BackGroundColorRuleValidator : IReportRuleValidator
    {
        public EmailValidationResult Validate(EmailReport report, ReportRule rule)
        {
            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(report.GetEmailMessage().Body);

            var body = htmlDocument.DocumentNode.Find(HtmlElementType.Body).FirstOrDefault();
            var blocks = htmlDocument.DocumentNode.Find("#block");
            var tables = htmlDocument.DocumentNode.Find(HtmlElementType.Table);
            var styleRegex = new Regex(@"background[\s]*:.*(!#fff|!#ffffff|white|transparent)");
            var isVisibleColor = new Func<string, bool>(x =>
                                                            {
                                                                var value = x.ToLower();
                                                                var hiddenValues = new string[] { "#fff", "#ffffff", "white", "transparent" };
                                                                return !string.IsNullOrEmpty(value) && !hiddenValues.Any(y => value.Contains(y));
                                                            });

            var valid = body == null ||
                        (body.Attributes.Any(x => x.Name.ToLower() == "bgcolor" && isVisibleColor(x.Value)) ||
                        body.Attributes.Any(x => x.Name.ToLower() == "style" && styleRegex.IsMatch(x.Value.ToLower())) ||
                        blocks.Any(block => block.Attributes.Any(x => x.Name.ToLower() == "style" && styleRegex.IsMatch(x.Value.ToLower()))) ||
                        blocks.Any(block => block.Attributes.Any(x => x.Name.ToLower() == "bgcolor" && isVisibleColor(x.Value))) ||
                        tables.Any(table => table.Attributes.Any(x => x.Name.ToLower() == "style" && styleRegex.IsMatch(x.Value.ToLower()))) ||
                        tables.Any(table => table.Attributes.Any(x => x.Name.ToLower() == "bgcolor" && isVisibleColor(x.Value))));

            
            return new EmailValidationResult
                       {
                           Valid = valid,
                           ReportRule = rule
                       };
        }
    }
}
﻿using ECO.Entities;

namespace ECO.ReportValidation
{
    public class FromNameRuleValidator : IReportRuleValidator
    {
        public EmailValidationResult Validate(EmailReport report, ReportRule rule)
        {
            return new EmailValidationResult {ReportRule = rule, Valid = false};
        }
    }
}

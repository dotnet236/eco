﻿using System.IO;
using System.Linq;
using ECO.Entities;
using HtmlAgilityPack;

namespace ECO.ReportValidation
{
    public class ImagesTextRuleValidator : IReportRuleValidator
    {
        public EmailValidationResult Validate(EmailReport report, ReportRule rule)
        {
            var document = new HtmlDocument();
            var body = report.GetEmailMessage().Body;
            // Get the HTML of the first half of the body text
            document.LoadHtml(body.Substring(0, body.Length / 2));

            // Get all image tags in there
            var imageTags = document.DocumentNode.Find(HtmlElementType.Img);

            var valid = !imageTags.Any(x => (!rule.Dictionary.Any(dict => x.Attributes["src"].Value != dict.Value))
                                            && (x.Attributes.Any(y => y.Name == "alt" && string.IsNullOrEmpty(y.Value))
                                            || !x.Attributes.Any(y => y.Name == "alt")));

            return new EmailValidationResult
                       {
                           ReportRule = rule,
                           Valid = valid
                       };
        }
    }
}
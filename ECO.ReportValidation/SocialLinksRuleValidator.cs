﻿using System.Linq;
using ECO.Entities;
using HtmlAgilityPack;

namespace ECO.ReportValidation
{
    public class SocialLinksRuleValidator : IReportRuleValidator
    {
        public EmailValidationResult Validate(EmailReport report, ReportRule rule)
        {
            var document = new HtmlDocument();
            document.LoadHtml(report.GetEmailMessage().Body);

            var anchorTags = document.DocumentNode.Find(HtmlElementType.A);
            var valid = anchorTags.Any(tag => rule.Dictionary.Any(entry => tag.Attributes.Any(a => a.Name.ToLower() == "href" && a.Value.ToLower().Contains(entry.Value.ToLower()))));
            var first = anchorTags.FirstOrDefault(tag => rule.Dictionary.Any(entry => tag.Attributes.Any(a => a.Name.ToLower() == "href" && a.Value.ToLower().Contains(entry.Value.ToLower()))));
            return new EmailValidationResult {ReportRule = rule, Valid = valid};
        }
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using ECO.Entities;
using HtmlAgilityPack;

namespace ECO.ReportValidation
{
    public class ImageSizeRuleValidator : IReportRuleValidator
    {
        public EmailValidationResult Validate(EmailReport report, ReportRule rule)
        {

            var document = new HtmlDocument();
            var body = report.GetEmailMessage().Body;
            
            document.LoadHtml(body);

            // Get all image tags in there
            var imageTags = document.DocumentNode.Find(HtmlElementType.Img);

            var valid = true;
            // For each image tag
            foreach(var imageTag in imageTags)
            {
                var src = imageTag.Attributes["src"];
                if (src == null || string.IsNullOrEmpty(src.Value))
                    continue;

                // If supported by the webserver, get the content length.
                // This way we don't have to download the image!
                try
                {
                    var href = new Uri(src.Value);
                    System.Net.WebRequest req = System.Net.HttpWebRequest.Create(href);
                    req.Method = "HEAD";
                    System.Net.WebResponse resp = req.GetResponse();
                    int ContentLength;
                    if (int.TryParse(resp.Headers.Get("Content-Length"), out ContentLength))
                    {
                        if (ContentLength > 256000) valid = false;
                    }
                }
                catch (Exception) { }
            }

            return new EmailValidationResult
                       {
                           Valid = valid,
                           ReportRule = rule
                       };
        }
    }
}
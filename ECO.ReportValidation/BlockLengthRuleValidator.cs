﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using ECO.Entities;
using ECO.ReportValidation;

namespace ECO.ReportValidation
{
    public class BlockLengthRuleValidator : IReportRuleValidator
    {
        public EmailValidationResult Validate(EmailReport report, ReportRule rule)
        {
            var maxWordCount = 200;
            var maxCharCount = 1000;
            var paragraphRegex = new Regex("<p.*>.*</p>");
            var blockRegex = new Regex("<table.*id=.*BLOCK.*>.*</table>");
            var htmlRegex = new Regex("<.*?>", RegexOptions.Compiled);
            var body = report.GetEmailMessage().Body;
            var paragraphMatches = paragraphRegex.Matches(body);
            var blockMatches = blockRegex.Matches(body);

            var validate = new Func<Match, bool>(match => 
                {
                    var value = match.Value.StripHtml();
                    var words = value.Split(new[] {" "}, StringSplitOptions.RemoveEmptyEntries);
                    return words.Length < maxWordCount && value.Length < maxCharCount;
                });
            
            var valid = paragraphMatches.Cast<Match>().All(validate) && 
                        blockMatches.Cast<Match>().All(validate);

            return new EmailValidationResult
                       {
                           ReportRule = rule,
                           Valid = valid
                       };
        }
    }
}
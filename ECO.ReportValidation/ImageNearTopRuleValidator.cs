﻿using System.Text.RegularExpressions;
using HtmlAgilityPack;
using ECO.Entities;

namespace ECO.ReportValidation
{
    public class ImageNearTopRuleValidator : IReportRuleValidator
    {
        public EmailValidationResult Validate(EmailReport report, ReportRule rule)
        {
            var document = new HtmlDocument();
            var body = report.GetEmailMessage().Body;
            // Get the HTML of the first half of the body text
            document.LoadHtml(body.Substring(0, body.Length / 2));

            // Get all image tags in there
            var imageTags = document.DocumentNode.Find(HtmlElementType.Img);

            return new EmailValidationResult
                       {
                           ReportRule = rule,
                           // If there are any image tags, return true
                           Valid = (imageTags.Count > 0?true:false)
                       };
        }
    }
}
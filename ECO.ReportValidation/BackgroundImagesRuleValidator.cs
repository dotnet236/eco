﻿using System;
using System.Linq;
using ECO.Entities;
using HtmlAgilityPack;

namespace ECO.ReportValidation
{
    public class BackGroundImagesRuleValidator : IReportRuleValidator
    {
        public EmailValidationResult Validate(EmailReport report, ReportRule rule)
        {
            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(report.GetEmailMessage().Body);

            var body = htmlDocument.DocumentNode.Find(HtmlElementType.Body).FirstOrDefault();

            var valid = body == null || !body.Attributes.Any(x => x.Name.ToLower() == "background");

            return new EmailValidationResult
                       {
                           Valid = valid,
                           ReportRule = rule
                       };
        }
    }
}
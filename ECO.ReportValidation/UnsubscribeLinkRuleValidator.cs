﻿using System;
using System.Linq;
using ECO.Entities;

namespace ECO.ReportValidation
{
    public class UnsubscribeLinkRuleValidator : IReportRuleValidator
    {
        public EmailValidationResult Validate(EmailReport report, ReportRule rule)
        {
            return DictionaryValidator.ValidateRegion(report, rule, 0, 0.3);
        }
    }
}

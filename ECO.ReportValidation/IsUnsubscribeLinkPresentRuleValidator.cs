﻿using System;
using System.Linq;
using ECO.Entities;

namespace ECO.ReportValidation
{
    public class IsUnsubscribeLinkPresentRuleValidator : IReportRuleValidator
    {
        public EmailValidationResult Validate(EmailReport report, ReportRule rule)
        {
            return DictionaryValidator.ValidateRegion(report, rule, 0, 1.0);
        }
    }
}

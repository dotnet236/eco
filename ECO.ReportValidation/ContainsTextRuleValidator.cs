﻿using ECO.Entities;

namespace ECO.ReportValidation
{
    public class ContainsTextRuleValidator : IReportRuleValidator
    {
        public EmailValidationResult Validate(EmailReport report, ReportRule rule)
        {
            return new EmailValidationResult {ReportRule = rule, Valid = true};
        }
    }
}

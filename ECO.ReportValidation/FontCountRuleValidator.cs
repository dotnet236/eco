﻿using System.Text.RegularExpressions;
using ECO.Entities;

namespace ECO.ReportValidation
{
    public class FontCountRuleValidator : IReportRuleValidator
    {
        public EmailValidationResult Validate(EmailReport report, ReportRule rule)
        {
            var emailMessage = report.GetEmailMessage();
            var inlineFontsRegex = new Regex("<font.>.</font>", RegexOptions.IgnorePatternWhitespace | RegexOptions.IgnoreCase);
            var cssFontsRegex = new Regex("font-family:.", RegexOptions.IgnorePatternWhitespace | RegexOptions.IgnoreCase);

            var inlineFonts = inlineFontsRegex.Match(emailMessage.Body);
            var cssFonts = cssFontsRegex.Match(emailMessage.Body);

            var totalUniqueFontCount = inlineFonts.Captures.Count + cssFonts.Captures.Count;

            return new EmailValidationResult
                       {
                           ReportRule = rule,
                           Valid = totalUniqueFontCount < 3
                       };
        }
    }
}
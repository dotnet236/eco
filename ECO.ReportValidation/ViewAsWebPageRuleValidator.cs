﻿using System.Linq;
using System.Text.RegularExpressions;
using ECO.Entities;

namespace ECO.ReportValidation
{
    public class ViewAsWebPageRuleValidator : IReportRuleValidator
    {
        public EmailValidationResult Validate(EmailReport report, ReportRule rule)
        {
            var emailMessage = report.GetEmailMessage().Body.Replace('\n',' ');

            // Matches any dictionary entry text plus a link including the text 'click' somewhere further on (Having trouble viewing? <a href='blah'>Click here</a>)
            var valid = rule.Dictionary.Any(x => new Regex(x.Value.ToLower() + ".*<a.*click.*").Match(emailMessage.ToLower()).Success);

            return new EmailValidationResult
                       {
                           ReportRule = rule,
                           Valid = valid
                       };
        }
    }
}
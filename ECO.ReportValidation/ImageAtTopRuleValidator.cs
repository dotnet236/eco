﻿using System;
using System.Text.RegularExpressions;
using ECO.Entities;

namespace ECO.ReportValidation
{
    public class ImageAtTopRuleValidator : IReportRuleValidator
    {
        public EmailValidationResult Validate(EmailReport report, ReportRule rule)
        {
            var emailMessage = report.GetEmailMessage();
            var emailBody = emailMessage.Body;
            var topBody = emailBody.Substring(0, (int) Math.Ceiling(emailBody.Length*.1));
            var imageRegex = new Regex("<img .*>");

            return new EmailValidationResult
                       {
                           ReportRule = rule,
                           Valid = imageRegex.Match(topBody).Success
                       };
        }
    }
}

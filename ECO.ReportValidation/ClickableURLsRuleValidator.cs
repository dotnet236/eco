﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using ECO.Entities;
using HtmlAgilityPack;

namespace ECO.ReportValidation
{
    public class ClickableURLsRuleValidator : IReportRuleValidator
    {
        public EmailValidationResult Validate(EmailReport report, ReportRule rule)
        {
            var document = new HtmlDocument();
            document.LoadHtml(report.GetEmailMessage().Body);

            var isUrl = new Regex("http://([\\w+?\\.\\w+])+([a-zA-Z0-9\\~\\!\\@\\#\\$\\%\\^\\&amp;\\*\\(\\)_\\-\\=\\+\\\\\\/\\?\\.\\:\\;\\'\\,]*)?", RegexOptions.IgnoreCase); 

            var anchorTags = document.DocumentNode.Find(HtmlElementType.A);
            var valid = anchorTags.Any(x =>
            {
                var href = x.Attributes["href"];
                return href != null && isUrl.Match(href.Value).Success;
            });

            return new EmailValidationResult {ReportRule = rule, Valid = valid};
        }
    }
}
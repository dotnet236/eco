﻿using System;
using System.Linq;
using System.Collections.Generic;
using ECO.Entities;
using HtmlAgilityPack;

namespace ECO.ReportValidation
{
    public class ImageLinksRuleValidator : IReportRuleValidator
    {
        public EmailValidationResult Validate(EmailReport report, ReportRule rule)
        {
            var document = new HtmlDocument();
            var body = report.GetEmailMessage().Body;
            // Get the HTML of the first half of the body text
            document.LoadHtml(body.Substring(0, body.Length / 2));

            // Get all image tags in there
            var imageTags = document.DocumentNode.Find(HtmlElementType.Img);

            var valid = imageTags.Any(x => HtmlDocumentExtensions.Parent(x, HtmlElementType.A) != null);

            return new EmailValidationResult
                       {
                           Valid = valid,
                           ReportRule = rule
                       };
        }
    }
}
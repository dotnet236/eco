﻿using System.Linq;
using ECO.Entities;

namespace ECO.ReportValidation
{
    public class FromEmailAddressRuleValidator : IReportRuleValidator
    {
        public EmailValidationResult Validate(EmailReport report, ReportRule rule)
        {
            return new EmailValidationResult {ReportRule = rule, Valid = false};
        }
    }
}

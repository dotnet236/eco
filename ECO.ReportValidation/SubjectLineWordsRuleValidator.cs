﻿using System;
using System.Collections.Generic;
using System.Linq;
using ECO.Entities;

namespace ECO.ReportValidation
{
    public class SubjectLineWordsRuleValidator : IReportRuleValidator
    {
        public EmailValidationResult Validate(EmailReport report, ReportRule rule)
        {
            var emailMessage = report.GetEmailMessage();
            var subject = emailMessage.Subject.ToLower();
            var valid = !rule.Dictionary.Any(dictionary => subject.Contains(dictionary.Value.ToLower()));
            return new EmailValidationResult
                       {
                           ReportRule = rule,
                           Valid = valid
                       };
        }
    }
}

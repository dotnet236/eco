﻿using System;
using System.Linq;
using System.Collections.Generic;
using ECO.Entities;
using HtmlAgilityPack;

namespace ECO.ReportValidation
{
    public class ImageWidthRuleValidator : IReportRuleValidator
    {
        public EmailValidationResult Validate(EmailReport report, ReportRule rule)
        {

            var document = new HtmlDocument();
            var body = report.GetEmailMessage().Body;
            document.LoadHtml(body);

            // Get all image tags in there
            var imageTags = document.DocumentNode.Find(HtmlElementType.Img);

            var valid = true;
            
            // For each image tag
            foreach(var imageTag in imageTags)
            {
                var widthAttr = imageTag.Attributes["width"];
                if (widthAttr == null || string.IsNullOrEmpty(widthAttr.Value))
                    continue;
                
                float width;
                if(float.TryParse(widthAttr.Value, out width) && width > 610) {
                    valid = false;
                }
            }

            return new EmailValidationResult
                       {
                           Valid = valid,
                           ReportRule = rule
                       };
        }
    }
}
﻿using System;
using System.Linq;
using ECO.Entities;

namespace ECO.ReportValidation
{
    public class DictionaryValidator
    {
        public static EmailValidationResult Validate(EmailReport report, ReportRule rule)
        {
            var emailMessage = report.GetEmailMessage();
            var valid = rule.Dictionary.Any(dictionary => emailMessage.Body.ToLower().Contains(dictionary.Value.ToLower()));

            return new EmailValidationResult
                       {
                           ReportRule = rule,
                           Valid = valid
                       };
        }

        public static EmailValidationResult ValidateRegion(EmailReport report, ReportRule rule, double start, double end)
        {
            var emailMessage = report.GetEmailMessage();
            var emailBody = emailMessage.Body;
            var region = emailBody.GetRegion(start, end);
            var valid = rule.Dictionary.Any(dictionary => region.ToLower().Contains(dictionary.Value.ToLower()));

            return new EmailValidationResult
                       {
                           ReportRule = rule,
                           Valid = valid
                       };
        }
    }

}

﻿using System;
using System.Linq;
using ECO.Entities;
using HtmlAgilityPack;

namespace ECO.ReportValidation
{
    public class PlaceholderImagesRuleValidator : IReportRuleValidator
    {
        public EmailValidationResult Validate(EmailReport report, ReportRule rule)
        {
            var emailMessage = report.GetEmailMessage();
            var valid = !rule.Dictionary.Any(dictionary => emailMessage.Body.ToLower().Contains(dictionary.Value.ToLower()));

            return new EmailValidationResult
            {
                ReportRule = rule,
                Valid = valid
            };
        }
    }
}

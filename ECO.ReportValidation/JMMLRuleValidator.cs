﻿using System.Text.RegularExpressions;
using ECO.Entities;

namespace ECO.ReportValidation
{
    public class JMMLRuleValidator : IReportRuleValidator
    {
        public EmailValidationResult Validate(EmailReport report, ReportRule rule)
        {
            return DictionaryValidator.Validate(report, rule);
        }
    }
}
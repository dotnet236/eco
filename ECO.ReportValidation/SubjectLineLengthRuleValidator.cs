﻿using System.Linq;
using ECO.Entities;

namespace ECO.ReportValidation
{
    public class SubjectLineLengthRuleValidator : IReportRuleValidator
    {
        public EmailValidationResult Validate(EmailReport report, ReportRule rule)
        {
            var emailMessage = report.GetEmailMessage();
            return new EmailValidationResult
                       {
                           ReportRule = rule,
                           Valid = emailMessage.Subject.Count() < 50
                       };
        }
    }
}

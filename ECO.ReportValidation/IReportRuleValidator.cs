﻿using ECO.Entities;

namespace ECO.ReportValidation
{
    public interface IReportRuleValidator
    {
        EmailValidationResult Validate(EmailReport report, ReportRule rule);
    }
}

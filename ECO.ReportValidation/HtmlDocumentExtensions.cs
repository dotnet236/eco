﻿using System.Collections.Generic;
using HtmlAgilityPack;

namespace ECO.ReportValidation
{
    public static class HtmlDocumentExtensions
    {
        public static IList<HtmlNode> Find(this HtmlNode node, HtmlElementType nodeType)
        {
            if (node.Name.ToLower() == nodeType.ToString().ToLower())
                return new List<HtmlNode> { node };

            var list = new List<HtmlNode>();
            foreach (var childNode in node.ChildNodes)
                list.AddRange(Find(childNode, nodeType));

            return list;
        }

        public static IList<HtmlNode> Find(this HtmlNode node, string expression)
        {
            if (string.IsNullOrEmpty(expression)) return new List<HtmlNode>();

            var quantifier = expression[0];
            switch (quantifier)
            {
                case '#':
                    if (node.Id == expression.Substring(1))
                        return new List<HtmlNode> { node };
                    break;
            }

            var list = new List<HtmlNode>();
            foreach (var childNode in node.ChildNodes)
                list.AddRange(Find(childNode, expression));

            return list;
        }

        public static HtmlNode Parent(this HtmlNode node, HtmlElementType nodeType)
        {
            if (node.Name.ToLower() == nodeType.ToString().ToLower())
                return node;

            if (node.Name.ToLower() == "body" || node.ParentNode == null)
                return null;

            return Parent(node.ParentNode, nodeType);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ECO.Entities;
using System.Text.RegularExpressions;

namespace ECO.ReportValidation
{
    public class IsPhysicalAddressPresentRuleValidator : IReportRuleValidator
    {
        public EmailValidationResult Validate(EmailReport report, ReportRule rule)
        {
            var bottomRegion = report.GetEmailMessage().Body.GetRegion(0.8, 1.0);
            var zipCodeRegex = new Regex(@"[\d]{5}|[\d]{5}\-[\d]{4}");

            bottomRegion = StripHtml(bottomRegion);

            return new EmailValidationResult
            {
                ReportRule = rule,
                Valid = zipCodeRegex.Match(bottomRegion).Success
            };
        }

        public string StripHtml(string bottomRegion)
        {
            bottomRegion = bottomRegion.StripHtml();

            

            return bottomRegion;
        }
    }
}

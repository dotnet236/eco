﻿namespace ECO.ReportValidation
{
    public enum HtmlElementType
    {
        Body,
        A,
        Img,
        Table
    }
}
﻿using System.Text.RegularExpressions;
using ECO.Entities;

namespace ECO.ReportValidation
{
    public class FontStylesRuleValidator : IReportRuleValidator
    {
        public EmailValidationResult Validate(EmailReport report, ReportRule rule)
        {
            var emailMessage = report.GetEmailMessage();
            var strongRegex = new Regex("<strong>", RegexOptions.IgnoreCase);
            var inlineBoldRegex = new Regex("<b>", RegexOptions.IgnoreCase);
            var cssBoldRegex = new Regex("font-weight:bold;", RegexOptions.IgnoreCase);

            var inlineItalicsRegex = new Regex("<i>.*</i>", RegexOptions.IgnoreCase);
            var cssItalicsRegex = new Regex("font-style:.italic;", RegexOptions.IgnoreCase);

            var inlineUnderlineRegex = new Regex("<u>.</u>", RegexOptions.IgnoreCase);
            var underlineRegex = new Regex("text-decoration:.underline;", RegexOptions.IgnoreCase);

            return new EmailValidationResult
                       {
                           ReportRule = rule,
                           Valid = strongRegex.Match(emailMessage.Body).Success ||
                                   inlineBoldRegex.Match(emailMessage.Body).Success ||
                                   cssItalicsRegex.Match(emailMessage.Body).Success ||
                                   inlineItalicsRegex.Match(emailMessage.Body).Success ||
                                   cssBoldRegex.Match(emailMessage.Body).Success ||
                                   inlineUnderlineRegex.Match(emailMessage.Body).Success ||
                                   underlineRegex.Match(emailMessage.Body).Success
                       };
        }
    }
}
﻿using System;
using System.IO;
using ECO.Entities;
using HtmlAgilityPack;

namespace ECO.ReportValidation
{
    public class MessageLengthRuleValidator : IReportRuleValidator
    {
        public EmailValidationResult Validate(EmailReport report, ReportRule rule)
        {
            var emailMessage = report.GetEmailMessage();
            var htmlDocument = new HtmlDocument();
            htmlDocument.Load(new StringReader(emailMessage.Body));
            var bodyWords = htmlDocument.DocumentNode.InnerText.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);

            return new EmailValidationResult
                       {
                           ReportRule = rule,
                           Valid = bodyWords.Length < 500
                           //Valid = bodyWords.Length < 200
                       };
        }
    }
}
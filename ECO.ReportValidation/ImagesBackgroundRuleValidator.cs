﻿using System.IO;
using System.Linq;
using ECO.Entities;
using HtmlAgilityPack;

namespace ECO.ReportValidation
{
    public class ImagesBackgroundRuleValidator : IReportRuleValidator
    {
        public EmailValidationResult Validate(EmailReport report, ReportRule rule)
        {
            var emailMessage = report.GetEmailMessage();
            var htmlDocument = new HtmlDocument();
            htmlDocument.Load(new StringReader(emailMessage.Body));

            var images = htmlDocument.DocumentNode.SelectNodes("//img");
            return new EmailValidationResult
                       {
                           ReportRule = rule,
                           Valid = !images.Any(x => !x.XPath.Contains("href"))
                       };
        }
    }
}
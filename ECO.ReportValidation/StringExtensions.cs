﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ECO.ReportValidation
{
    public static class StringExtensions
    {
        public static string GetRegion(this string text, double start, double end)
        {
            end = Math.Min(1.0, end);
            var startPt = (int)Math.Floor(text.Length * start);
            var endPt = (int)Math.Floor(text.Length * end);
            return text.Substring(startPt, endPt - startPt);
        }

        public static string StripHtml(this string text)
        {
            text = Regex.Replace(text, "<(.|\n)*?>", string.Empty);
            var tagIndex = text.IndexOf('>');
            if (tagIndex > 0)
                text = text.Substring(tagIndex);
            return text;
        }
    }
}

﻿using System.Collections.Generic;
using FluentNHibernate.Data;
using NHibernate;
using NHibernate.Criterion;

namespace ECO.Repositories.Base
{
    public class BaseRepository : IBaseRepository
    {
        private readonly ISession _session;

        public BaseRepository(ISession session)
        {
            _session = session;
        }

        public ITransaction BeginTransaction()
        {
            return _session.BeginTransaction();
        }

        public virtual T SaveOrUpdate<T>(T entity, ITransaction trans) where T : Entity
        {
            var newTransaction = trans == null;
            if (newTransaction)
                trans = _session.BeginTransaction();

            _session.SaveOrUpdate(entity);

            if (newTransaction)
            {
                trans.Commit();
                trans.Dispose();
            }

            return entity;
        }

        public T Insert<T>(T entity, ITransaction trans) where T : Entity
        {
            var newTransaction = trans == null;
            if (newTransaction)
                trans = _session.BeginTransaction();

            _session.Save(entity);

            if (newTransaction)
            {
                trans.Commit();
                trans.Dispose();
            }

            return entity;
        }

        public IList<T> SaveOrUpdate<T>(IList<T> entities, ITransaction trans) where T : Entity
        {
            var newTransaction = trans == null;
            if (newTransaction)
                trans = _session.BeginTransaction();

            foreach (var entity in entities)
                _session.SaveOrUpdate(entity);

            if (newTransaction)
            {
                trans.Commit();
                trans.Dispose();
            }

            return entities;
        }

        public virtual T Get<T>(int id) where T : Entity
        {
            return _session.Get<T>(id);
        }

        public T Get<T>(DetachedCriteria seachCriteria) where T : Entity
        {
            return seachCriteria.GetExecutableCriteria(_session).UniqueResult<T>();
        }

        public virtual T Get<T>(long id) where T : Entity
        {
            return _session.Get<T>(id);
        }

        public void Delete(Entity entity, ITransaction trans)
        {
            var newTransaction = trans == null;
            if (newTransaction)
                trans = _session.BeginTransaction();

            _session.Delete(entity);

            if (!newTransaction)
                return;

            trans.Commit();
            trans.Dispose();
        }

        public virtual IEnumerable<T> List<T>(DetachedCriteria detachedCriteria) where T : Entity
        {
            return detachedCriteria.GetExecutableCriteria(_session).List<T>();
        }
    }
}
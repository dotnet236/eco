﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ECO.Entities;
using ECO.ReportValidation;
using ECO.Repositories.Base;
using NHibernate.Criterion;

namespace ECO.Services
{
    public class EmailReportService : IEmailReportService
    {
        private readonly IBaseRepository _baseRepository;

        private IEnumerable<Type> _reportRuleValidators;
        public IEnumerable<Type> ReportRuleValidators
        {
            get
            {
                if (_reportRuleValidators == null)
                {
                    var iReportValidatorType = typeof(IReportRuleValidator);
                    var reportValidationTypes = Assembly.GetAssembly(iReportValidatorType).GetTypes();
                    _reportRuleValidators = reportValidationTypes.Where(iReportValidatorType.IsAssignableFrom);
                }
                return _reportRuleValidators;
            }
        }

        public EmailReportService(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public EmailReport SaveEmailReport(EmailReport newEmailReport)
        {
            return _baseRepository.SaveOrUpdate(newEmailReport);
        }

        public EmailReport GetEmailByHalfOfReportId(string halfOfReportId)
        {
            var criteria = DetachedCriteria.For<EmailReport>();
            criteria = criteria.Add(Restrictions.InsensitiveLike("ReportId", halfOfReportId, MatchMode.Start));
            return _baseRepository.Get<EmailReport>(criteria);
        }

        public EmailReport GetReportById(string reportId)
        {
            var criteria = DetachedCriteria.For<EmailReport>();
            criteria = criteria.Add(Restrictions.Eq("ReportId", reportId));
            return _baseRepository.Get<EmailReport>(criteria);
        }

        public ReportTopic GetTopicById(long id)
        {
            return _baseRepository.Get<ReportTopic>(id);
        }

        public IList<ReportSection> GetSections()
        {
            return _baseRepository.List<ReportSection>();
        }

        public EmailValidationResult ValidateRule(EmailReport report, ReportRule rule)
        {
            var validator = GetReportRuleValidator(rule.Name);
            return validator == null
                       ? new EmailValidationResult {ReportRule = rule, Valid = false}
                       : validator.Validate(report, rule);
        }

        public ReportTopic SaveTopic(ReportTopic topic)
        {
            return _baseRepository.SaveOrUpdate(topic);
        }

        public ReportRule GetRuleById(long id)
        {
            return _baseRepository.Get<ReportRule>(id);
        }

        public ReportRule SaveRule(ReportRule rule)
        {
            return _baseRepository.SaveOrUpdate(rule);
        }

        public ReportSection SaveSection(ReportSection section)
        {
            return _baseRepository.SaveOrUpdate(section);
        }

        private IReportRuleValidator GetReportRuleValidator(string ruleName)
        {
            var validatorName = string.Format("{0}RuleValidator", ruleName).ToLower();
            var validator = ReportRuleValidators.FirstOrDefault(x => x.Name.ToLower() == validatorName);
            if (validator == null)
                return null;
                //throw new Exception("Rule validator " + ruleName + " has not been implemented.  Expected type of " + validatorName + " in ECO.ReportValidation.");
            return Activator.CreateInstance(validator) as IReportRuleValidator;
        }
    }
}

﻿using System.Collections.Generic;
using ECO.Entities;

namespace ECO.Services
{
    public interface IEmailReportService
    {
        EmailReport SaveEmailReport(EmailReport newEmailReport);
        EmailReport GetReportById(string reportId);
        IList<ReportSection> GetSections();
        EmailValidationResult ValidateRule(EmailReport report, ReportRule rule);
        ReportTopic GetTopicById(long id);
        ReportTopic SaveTopic(ReportTopic topic);
        ReportRule GetRuleById(long id);
        ReportRule SaveRule(ReportRule rule);
        ReportSection SaveSection(ReportSection section);
    }
}
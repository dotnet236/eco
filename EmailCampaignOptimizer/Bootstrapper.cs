﻿using System.Web.Mvc;
using EmailCampaignOptimizer.StructureMap;
using StructureMap;

namespace EmailCampaignOptimizer
{
    public class Bootstrapper
    {
        public static void Initialize()
        {
            ConfigureStructureMap();
        }

        private static void ConfigureStructureMap()
        {
            ObjectFactory.Initialize(x => x.AddRegistry(new ApplicationRegistry()));
            ControllerBuilder.Current.SetControllerFactory(new StructureMapControllerFactory());
            //HibernatingRhinos.Profiler.Appender.NHibernate.NHibernateProfiler.Initialize(); 
        }
    }
}
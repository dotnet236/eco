function dcontains(str, arr) {
    if ((str == undefined) || (arr == undefined)) {
        return false;
    }
    for (i in arr) {
        if (str.indexOf(arr[i].Value.toLowerCase()) >= 0) { return true; }
    }
    return false;
}

function scontains(str, phrase) {
    if (str == undefined) { return false; }    
    if (str.indexOf(phrase) >= 0) { return true; }
    return false;
}

function pmatch(str, pat) {
    if ((str != undefined) && (str != null) &&
        pat.test(str)) {
        return true;
    }
    return false;
}

function getregion(str, start, end) {
    if ((str == undefined) || (str == null)) { return ""; }
    return str.substring(start * str.length, end * str.length);
}

function countmatches(str, pat) {
    var n = 0;
    var pos = 0;
    while (true) {
        pos = str.indexOf(pat, pos);
        if (pos != -1) { n++; pos += pat.length; }
        else { break; }
    }
    return n;
}

function validate(email, rule)  {
    if (rule.Name == 'backgroundcolor') {
        var body = $(email.Body);
        var blocks = body.find('block');
        var tables = body.find('table');
        var patStyle = RegExp('background[\s]*:.*(#fff|#ffffff|white|transparent)');
        var patBgColor = RegExp('(#fff|#ffffff|white|transparent)');
        if (pmatch(body.attr('bgcolor'), patBgColor)) { return false; }
        if (pmatch(body.attr('style'), patStyle)) { return false; }
        $.each(blocks, function (key, value) {
            var block = $(value);
            if (pmatch(block.attr('bgcolor'), patBgColor)) { return false; }
            if (pmatch(block.attr('style'), patStyle)) { return false; }
        });
        $.each(tables, function (key, value) {
            var table = $(value);
            if (pmatch(table.attr('bgcolor'), patBgColor)) { return false; }
            if (pmatch(table.attr('style'), patStyle)) { return false; }
        });
        return true;
    }

    if (rule.Name == 'backgroundimages') {
        var body = $(email.Body);
        if (body.attr('background') == undefined) {
            return true;
        }
        return false;
    }

    if (rule.Name == 'blocklength') {
        var body = $(email.Body);
        var tables = body.find('table');
        var paras = body.find('p');
        var num = 0;

        $.each(tables, function (key, value) {
            var table = $(value);
            if (scontains(table.attr('id'), 'BLOCK')) {
                num += table.text().length;
            }
        });
        if (num > 1000) { return false; }

        $.each(paras, function (key, value) {
            num += $(value).text().length;
        });
        if (num > 1000) { return false; }
        return true;
    }

    if (rule.Name == 'clickableurls') {
        var body = $(email.Body);
        var anchors = body.find('a');
        $.each(anchors, function (key, value) {
            var href = $(value).attr('href');
            if ((href != undefined) &&
                (href.toLowerCase().substr(0, 4) != 'http')) {
                return false;
            }
        });
        return true;
    }

    if (rule.Name == 'containstext') { return true; }

    if (rule.Name == 'fontcount') {
        var body = $(email.Body);
        var fonts = body.find('font');
        var count = fonts.length;
        count += countmatches(email.Body, 'font-family:');
        if (count < 3) { return true; }
        return false;
    }

    if (rule.Name == 'fontstyles') {
        if (scontains(email.Body, '<strong>')) { return true; }
        if (scontains(email.Body, '<b>')) { return true; }
        if (scontains(email.Body, 'font-weight:bold')) { return true; }
        if (scontains(email.Body, '<i>')) { return true; }
        if (scontains(email.Body, '<u>')) { return true; }
        if (scontains(email.Body, 'text-decoration:underline')) { return true; }
        return false;
    }

    if (rule.Name == 'forwardlink') {
        return dcontains(email.Body.toLowerCase(), rule.Dictionary);
    }

    if (rule.Name == 'friendlylinks') {
        var body = $(email.Body);
        var anchors = body.find('a');
        var ret = true;
        $.each(anchors, function (key, value) {
            if (ret) {
                var href = $(value).attr('href');
                if ((href != undefined) &&
                    (href.toLowerCase().substr(0, 4) != 'http')) {
                    ret = false;
                }
                if (dcontains($(value).text().toLowerCase(), rule.Dictionary)) {
                    ret = false;
                }
            }
        });
        return ret;
    }

    if (rule.Name == 'fromname') {
        return false;
    }

    if (rule.Name == 'fromemailaddress') {
        return false; 
    }

    if (rule.Name == 'isphysicaladdresspresent') {
        //var region = getregion(email.Body, 0.7, 1.0);
        return pmatch($(email.Body).text(), RegExp('\\D[\\d]{5}\\D|\\D[\\d]{5}\\-[\\d]{4}'));
    }

    if (rule.Name == 'isunsubscribelinkpresent') {
        return dcontains(email.Body.toLowerCase(), rule.Dictionary);
    }

    if (rule.Name == 'industrystats') {
    }

    if (rule.Name == 'imagelinks') {
        var body = $(email.Body.substring(0, email.Body.length / 2));
        var images = body.find('img');
        var ret = false;
        $.each(images, function (key, value) {
            if (!ret) {
                var p = $(value).parent('a');
                if (p != undefined) {
                    ret = true; 
                }
            }
        });
        return ret;
    }

    if (rule.Name == 'imageneartop') {
        var body = $(email.Body.substring(0, email.Body.length / 2));
        var imageTags = body.find('img');
        if (imageTags.length > 0) {
            return true;
        }
        return false;
    }

    if (rule.Name == 'imagesize') {
        var body = $(email.Body);
        var images = body.find('img');
        var ret = true;
        $.each(images, function (key, value) {
            if (ret) {
                var w = $(value).attr('width');
                var h = $(value).attr('height');
                if ((w != undefined) && (h != undefined)) {
                    if (((w * h) / 3) > 256000) {
                        ret = false;
                    }
                }
            }
        });
        return ret;
    }

    if (rule.Name == 'imagestext') {
        var body = $(email.Body);
        var images = body.find('img');
        var ret = false;
        $.each(images, function (key, value) {
            if (!ret) {
                var img = $(value);
                if ((img.attr('src') != undefined) &&
                    !dcontains(img.attr('src').toLowerCase(), rule.Dictionary) &&
                    (img.attr('alt') != undefined)) {
                    ret = true; 
                }
            }
        });
        return ret;
    }

    if (rule.Name == 'imagewidth') {
        var body = $(email.Body);
        var images = body.find('img');
        var ret = true;
        $.each(images, function (key, value) {
            if (ret) {
                var w = $(value).attr('width');
                if ((w != undefined) && (w > 610)) {
                    ret = false;
                }
            }
        });
        return ret;
    }

    if (rule.Name == 'jmml') {
        return dcontains(email.Body.toLowerCase(), rule.Dictionary);
    }

    if (rule.Name == 'messagelength') {
        var body = $(email.Body);
        if (body.text().length < 2500) { return true; }
        return false;
    }

    if (rule.Name == 'placeholderimages') {
        if (dcontains(email.Body.toLowerCase(), rule.Dictionary)) {
            return false;
        }
        return true;
    }

    if (rule.Name == 'sharebar') {
        var body = $(email.Body);
        var anchors = body.find('a');
        var ret = false;
        $.each(anchors, function (key, value) {
            if (!ret) {
                var href = $(value).attr('href');
                if ((href != undefined) &&
                    dcontains(href.toLowerCase(), rule.Dictionary)) {
                    ret = true;
                }
            }
        });
        return ret;
    }

    if (rule.Name == 'sociallinks') {
        var body = $(email.Body);
        var anchors = body.find('a');
        var ret = false;
        $.each(anchors, function (key, value) {
            if (!ret) {
                var href = $(value).attr('href');
                if ((href != undefined) &&
                    dcontains(href.toLowerCase(), rule.Dictionary)) {
                    ret = true;
                }
            }
        });
    }

    if (rule.Name == 'subjectlinelength') {
        if ((email.Subject != undefined) &&
            (email.Subject.length < 50)) {
            return true;
        }
        return false;
    }

    if (rule.Name == 'subjectlinespam') {
        var subject = email.Subject.toLowerCase();
        return !dcontains(subject, rule.Dictionary);
    }

    if (rule.Name == 'subjectlinewords') {
        var subject = email.Subject.toLowerCase();
        return !dcontains(subject, rule.Dictionary);
    }

    if (rule.Name == 'unsubscribelink') {
        var region = getregion(email.Body, 0, 0.3);
        return dcontains(region, rule.Dictionary);        
    }

    if (rule.Name == 'viewaswebpage') {
        var body = email.Body.replace('\n', ' ').toLowerCase();
        for (i in rule.Dictionary) {
            var pat = rule.Dictionary[i].Value.toLowerCase() + ".*<a.*click.*";
            if (RegExp(pat).test(body)) {
                return true;
            }
        } 
        return false;
    }

    if (rule.Name == 'whitelistingrequest') {
        var body = email.Body.toLowerCase();
        return dcontains(body, rule.Dictionary);
    }

    if (rule.Name == 'yourstats') {
    }

    return false;
}



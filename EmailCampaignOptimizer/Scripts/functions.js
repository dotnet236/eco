function mapActions() {
    // landing
    $('#ctct-cstmr').click(function () {
        $('#opt-create-info1').slideToggle("fast",
			function () {
			    $('#opt-create-info2').hide();
			});
    });
    $('#ctct-noncstmr').click(function () {
        $('#opt-create-info2').slideToggle("fast",
		function () {
		    $('#opt-create-info1').hide();
		});
    });

    // report
    $('ul#report-summary div.report-hdr').click(function () {
        $(this)
			.toggleClass('report-dtl-active')
			.next('dl.report-dtl')
			.slideToggle('fast');
    });

    // expand all
    $('a#eco-expand').click(function () {
        $('ul#report-summary div.report-hdr')
			.addClass('report-dtl-active')
			.next('dl.report-dtl')
			.slideDown('fast');
        return false;
    });

    // collapse all
    $('a#eco-collapse').click(function () {
        $('ul#report-summary div.report-hdr')
			.removeClass('report-dtl-active')
			.next('dl.report-dtl')
			.slideUp('fast');
        return false;
    });

    // learn more
    var learnmore = $('div.learnmoretext');
    learnmore.hide();
    $('a.learnmorelink').click(function () {
        $(this).toggleClass('learnmore-active');
        if ($(this).data('shown')) {
            $(this).next(learnmore).fadeOut('medium');
            $(this).data('shown', false);
        } else {
            $(this).next(learnmore).fadeIn('slow');
            $(this).data('shown', true);
        }
        return false;
    });

    // show other industry text field
    if ($("#industryselector").length != 0) {
        $("#industryselector").change(function () {
            var val = $(this).val();
            if (val == 'other') {
                $('#otherindustryinput').show();
            } else {
                $('#otherindustryinput').hide();
            }
        }).change();
    }
    if ($("#productselector").length != 0) {
        $("#productselector").change(function () {
            var val = $(this).val();
            if (val == 'other') {
                $('#otherproductinput').show();
            } else {
                $('#otherproductinput').hide();
            }
        }).change();
    }
}

$(function () {
    mapActions();
});
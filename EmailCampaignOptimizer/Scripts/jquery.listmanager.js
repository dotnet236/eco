﻿(function ($) {

    $.fn.listManager = function () {

        return this.each(function () {

            var listManager = $(this);
            var list = listManager.find("ul");
            var form = listManager.find("form").first();

            var manageHeader = $("<div />");
            var input = $("<input type='textbox' />");
            var addButton = $("<input type='button' />");

            manageHeader.append(input);
            manageHeader.append(addButton);
            form.prepend(manageHeader);

            function removeLink() {
                var removeLink = $("<a href='javascript:'>");
                removeLink.text("remove");
                removeLink.click(function () { $(this).parents("li").remove(); });
                return removeLink;
            }

            function hiddenInput(value) {
                var hiddenInput = $("<input type='hidden' name='listItem' />");
                hiddenInput.val(value);
                return hiddenInput;
            }

            addButton.val("Add");
            addButton.click(function () {
                if ($.trim(input.val()) == "") return;
                var newItem = $("<li />");
                var newItemLabel = $("<label />");
                newItemLabel.text(input.val());
                newItem.append(newItemLabel);
                newItem.append(removeLink());
                newItem.append(hiddenInput(input.val()));
                list.append(newItem);
                input.val("");
            });

            list.find("label").each(function () {
                var label = $(this);
                label.after(removeLink());
                label.after(hiddenInput(label.text()));
            });

            list.before(manageHeader);
        });

    };

    $(function () {
        $(".listManager").listManager();
    });

})(jQuery);

﻿(function ($) {

    $.fn.clearFormLink = function () {

        return this.each(function () {

            var link = $(this);
            link.click(function () {
                var answer = confirm("Are you sure? There is no undo.");
                if (answer) {
                    var form = link.parents("form");
                    var inputs = form.children("[class^=editable]");
                    inputs.each(function () {
                        var input = $(this);
                        var type = input.attr("class").split("editable")[1].toLowerCase();
                        var name = input.attr("name");
                        function clearField(dynamicInput) {
                            function close() {
                                dynamicInput.remove();
                                input.show();
                                link.show();
                            }
                            link.hide();
                            input.hide().after(dynamicInput);
                            form.ajaxForm(function (result) {
                                input.html(dynamicInput.val());
                                close();
                                $("#ajaxMessageBox").html(result)
                            });
                            form.submit();
                        }
                        var textBox = $("<input type='hidden'/>");
                        textBox.attr("name", name);
                        textBox.val('');
                        clearField(textBox);
                    });
                }
            });
        });
    };

})(jQuery);
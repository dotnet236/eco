﻿(function ($) {

    $.fn.editFormLink = function () {

        return this.each(function () {

            var link = $(this);
            link.click(function () {
                var form = link.parents("form");
                var inputs = form.children("[class^=editable]");
                inputs.each(function () {
                    var input = $(this);
                    var type = input.attr("class").split("editable")[1].toLowerCase();
                    var name = input.attr("name");
                    var tinymce = input.data("tinymce");

                    function convertInput(dynamicInput, timymceEditable) {
                        var buttonContainer = $("<div />").addClass("buttonContainer");
                        var cancelButton = $("<input type='button' />").attr("value", "Cancel");
                        var submitButton = $("<input type='submit' />").attr("value", "save");

                        function close() {
                            dynamicInput.remove();
                            submitButton.remove();
                            cancelButton.remove();
                            input.show();
                            link.show();
                        }

                        cancelButton.click(close);
                        buttonContainer.append(cancelButton);
                        buttonContainer.append(submitButton);
                        link.hide();
                        input.hide().after(dynamicInput);
                        form.append(buttonContainer);
                        form.ajaxForm(function (result) {
                            input.html(dynamicInput.val());
                            close();
                            $("#ajaxMessageBox").html(result)
                        });
                        if (timymceEditable) {
                            dynamicInput.tinymce({
                                theme: "advanced",
                                theme_advanced_toolbar_location: "top",
                                theme_advanced_toolbar_align: "left",
                                theme_advanced_statusbar_location: "bottom",
                                theme_advanced_resizing: true,
                                theme_advanced_buttons1: "bold,italic,underline,|,justifyleft,justifycenter,justifyright,|,bullist,numlist,outdent,indent,|,forecolor,backcolor,|,link,unlink,image,|,removeformat,undo,|,code",
                                theme_advanced_buttons2: "",
                                theme_advanced_buttons3: ""
                            });
                        }
                    }

                    switch (type) {
                        case 'textbox':
                            var textBox = $("<input type='textbox' />");
                            textBox.attr("name", name);
                            textBox.val(input.text());
                            convertInput(textBox);
                            break;
                        case 'textarea':
                            var textBox = $("<textarea />");
                            textBox.attr("name", name);
                            textBox.attr("rows", 10);
                            textBox.attr("cols", 50);
                            textBox.val(input.html().trim());
                            convertInput(textBox, tinymce);
                            break;
                        case 'checkbox':
                            var checkbox = $("<input type='checkbox' />");
                            checkbox.attr("name", name);
                            checkbox.attr("checked", input.text());
                            convertInput(checkbox);
                            break;
                    }
                });
            });
        });
    };

})(jQuery);
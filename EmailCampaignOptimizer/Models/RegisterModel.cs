﻿namespace EmailCampaignOptimizer.Models
{
    public class RegisterModel
    {
        public string EmailAddress { get; set; }
        public string Industry { get; set; }
        public string CreationApplication { get; set; }
    }
}
﻿namespace EmailCampaignOptimizer.Models
{
    public class RulesModel
    {
        public bool Success { get; set; }
        public string ReturnMessage { get; set; }
        public int RuleValue { get; set; }
    }
}
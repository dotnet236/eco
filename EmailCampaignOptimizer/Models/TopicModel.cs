﻿using System.Collections.Generic;

namespace EmailCampaignOptimizer.Models
{
    public class TopicModel
    {
        public string Title { get; set; }
        public string FromAddress { get; set; }
        public IList<RulesModel> Rules { get; set; }
        public string Tips { get; set; }
        public string Examples { get; set; }
        public string Resources { get; set; }
    }
}
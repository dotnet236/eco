﻿using System.Collections.Generic;

namespace EmailCampaignOptimizer.Models
{
    public class ReportModel
    {
        public string ReportId { get; set; }
        public IList<SectionModel> Sections { get; set; }
        public string Score { get; set; }
        public string EmailSubject { get; set; }
    }
}
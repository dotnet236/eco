﻿using System.Collections.Generic;

namespace EmailCampaignOptimizer.Models
{
    public class FullTopicModel
    {
        public string Title { get; set; }
        public string MoreInfoLink { get; set; }
        public string ExamplesLink { get; set; }
        public string AdditionalResources { get; set; }
        public string Tips { get; set; }
        public IList<FullRulesModel> ReportRules { get; set; }
    }
}
﻿using System.Collections.Generic;

namespace EmailCampaignOptimizer.Models
{
    public class FullRulesModel
    {
        public string Name { get; set; }
        public string SuccessMsg { get; set; }
        public string FailMsg { get; set; }
        public int Weight { get; set; }
        public IList<RuleDictionaryModel> Dictionary { get; set; }
        public bool Enabled { get; set; }
    }
}
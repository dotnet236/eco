﻿using System.Collections.Generic;

namespace EmailCampaignOptimizer.Models
{
    public class SectionModel
    {
        public string Title { get; set; }
        public long Id { get; set; }
        public IList<TopicModel> Topics { get; set; }
        public string Grade { get; set; }
    }
}
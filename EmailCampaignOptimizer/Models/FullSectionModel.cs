﻿using System.Collections.Generic;

namespace EmailCampaignOptimizer.Models
{
    public class FullSectionModel
    {
        public string Title { get; set; }
        public long Id { get; set; }
        public IList<FullTopicModel> Topics { get; set; }
        public int Percentage { get; set; }
    }
}
﻿using System;
using System.Reflection;
using ECO.Entities;
using ECO.Repositories.Base;
using ECO.Services;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Conventions.Helpers;
using NHibernate;
using StructureMap.Configuration.DSL;

namespace EmailCampaignOptimizer.StructureMap
{
    public class ApplicationRegistry : Registry
    {
        private static ISessionFactory SessionFactory
        {
            get
            {
                return Fluently.Configure()
                    .Database(MySQLConfiguration.Standard.ConnectionString(x =>
                    {
                        x.Server("localhost");
                        x.Database("ECO");
                        x.Username("root");
                        x.Password("root");
                    })).Diagnostics(x => x.OutputToConsole())
                 .Mappings(m => m.FluentMappings.AddFromAssemblyOf<EmailReportMap>()
                     .Conventions.Add(Table.Is(x => string.Format("{0}s",x.TableName.Replace("eco.", ""))))
                     .Conventions.Add(PrimaryKey.Name.Is(x =>
                     {
                         var fqn = x.EntityType.ToString();
                         var lastPeriodIndex = fqn.LastIndexOf('.') + 1;
                         if (lastPeriodIndex > fqn.Length) lastPeriodIndex = 0;
                         return String.Format("{0}Id", fqn.Substring(lastPeriodIndex));
                     })))
                    .BuildSessionFactory();
            }
        }

        public ApplicationRegistry()
        {
            ConfigureRepositories();
            ConfigureServies();
        }

        private void ConfigureServies()
        {
            Scan(x =>
            {
                x.TheCallingAssembly();
                x.Assembly(Assembly.GetAssembly(typeof(EmailReportService)));
                x.WithDefaultConventions();
            });
        }

        private void ConfigureRepositories()
        {
            For<IBaseRepository>().Use<BaseRepository>();
            For<ISessionFactory>().Singleton().Use(x => SessionFactory);
            For<ISession>().HttpContextScoped().Use(context => context.GetInstance<ISessionFactory>().OpenSession());
        }
    }
}
﻿using System.Web.Mvc;

namespace EmailCampaignOptimizer.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction("index", "optimize");
        }
    }
}

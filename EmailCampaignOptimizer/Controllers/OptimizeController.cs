﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ECO.Entities;
using ECO.Services;
using EmailCampaignOptimizer.Models;
using ECO.Configuration;
using AutoMapper;

namespace EmailCampaignOptimizer.Controllers
{
    public class OptimizeController : Controller
    {
        private readonly IEmailReportService _emailReportService;
        private const int EmailReportIdGuidLength = 7;

        public OptimizeController(IEmailReportService emailReportService)
        {
            _emailReportService = emailReportService;
        }

        public ActionResult Index()
        {
            return Register();
        }

        public ActionResult Register()
        {
            return View("Register", new RegisterModel());
        }

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            try
            {
                var emailReport = new EmailReport
                           {
                               ReportId = GetReportId(),
                               CreationApplication = model.CreationApplication,
                               Industry = model.Industry,
                               UserEmailAddress = model.EmailAddress,
                               Status = EmailStatus.Waiting
                           };
                emailReport = _emailReportService.SaveEmailReport(emailReport);
                var uniqueId = emailReport.ReportId;
                var emailAddress = string.Format("{0}@{1}", uniqueId, ECO.Configuration.Configuration.DomainName);
                return View("SendUsYourEmail", new SendUsYourEmailModel { EmailAddres = emailAddress });
            }
            catch (Exception ex)
            {
                return View("SendUsYourEmail", new SendUsYourEmailModel
                                                   {
                                                       EmailAddres = GetFullErrorMessagE(ex)
                                                   });
            }
        }

        private string GetFullErrorMessagE(Exception ex)
        {
            return ex.Message + (ex.InnerException == null ? "" : GetFullErrorMessagE(ex.InnerException));
        }

        private string GetReportId()
        {
            var reportId = Guid.NewGuid().ToString().Substring(0, EmailReportIdGuidLength);
            var emailReport = _emailReportService.GetReportById(reportId);
            return emailReport == null ? reportId : GetReportId();
        }

        public ActionResult GetReport(string reportId)
        {
            var sections = _emailReportService.GetSections();
            var report = _emailReportService.GetReportById(reportId);
            if (report.Email == null)
                throw new Exception("Email has not been received yet.");

            var model = new ReportModel { Sections = new List<SectionModel>(),  EmailSubject = report.GetEmailMessage().Subject };
            double reportSuccessGrade = 0;
            double reportTotalGrade = 0;
            foreach (var reportSection in sections)
            {
                var sectionsModel = new SectionModel { Topics = new List<TopicModel>(), Id = reportSection.Id, Title = reportSection.Title };
                double sectionSuccessGrade = 0;
                double sectionTotalGrade = 0;
                foreach (var topic in reportSection.Topics.OrderBy(x => x.Title))
                {
                    var rulesModel = (from rule in topic.ReportRules.Where(x => x.Enabled)
                                      let ruleResult = _emailReportService.ValidateRule(report, rule)
                                      select new RulesModel
                                                 {
                                                     ReturnMessage = ruleResult.Valid ? rule.SuccessMsg : rule.FailMsg,
                                                     Success = ruleResult.Valid,
                                                     RuleValue = rule.Weight
                                                 }).ToList();
                    sectionSuccessGrade += rulesModel.Where(x => x.Success).Sum(x => x.RuleValue);
                    sectionTotalGrade += rulesModel.Sum(x => x.RuleValue);
                    sectionsModel.Topics.Add(new TopicModel { Rules = rulesModel, Title = topic.Title, FromAddress = report.UserEmailAddress, Examples = topic.ExamplesLink, Tips = topic.MoreInfoLink, Resources = topic.AdditionalResources });
                }
                sectionsModel.Grade = ConvertPercentToLetterGrade(sectionSuccessGrade / sectionTotalGrade);
                reportSuccessGrade += sectionSuccessGrade*reportSection.Percentage;
                reportTotalGrade += sectionTotalGrade*reportSection.Percentage;
                model.Sections.Add(sectionsModel);
            }

            model.Score = ConvertPercentToLetterGrade(reportSuccessGrade/reportTotalGrade);

            return View("Report", model);
        }

        public ActionResult GetReportClient(string reportId)
        {
            var report = _emailReportService.GetReportById(reportId);
            if (report.Email == null)
                throw new Exception("Email has not been received yet.");
            var model = new ReportModel { ReportId = reportId, EmailSubject = report.GetEmailMessage().Subject };
            return View("ReportClient", model);
        }

        public ActionResult GetReportRaw(string reportId)
        {
            var report = _emailReportService.GetReportById(reportId);
            return Json(new { report = report }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetReportSections(string reportId)
        {
            var sections = _emailReportService.GetSections();
            var report = _emailReportService.GetReportById(reportId);
            Mapper.CreateMap<ReportRuleDictionary, RuleDictionaryModel>().ForMember(d => d.Value, opt => opt.MapFrom(s => s.Value.ToLower()));
            Mapper.CreateMap<ReportRule, FullRulesModel>();
            Mapper.CreateMap<ReportTopic, FullTopicModel>();
            Mapper.CreateMap<ReportSection, FullSectionModel>().ForMember(d => d.Topics, opt => opt.MapFrom(s => s.Topics.OrderBy(x => x.Title)));
            IList<FullSectionModel> sectionsModel = Mapper.Map<IList<ReportSection>, IList<FullSectionModel>>(sections);
            return Json(new { report = report, sections = sectionsModel }, JsonRequestBehavior.AllowGet);
        }

        private static string ConvertPercentToLetterGrade(double percentGrade)
        {
            percentGrade = percentGrade * 100;

            if (percentGrade > 100 || percentGrade < 0)
                throw new Exception("Invalid percent " + percentGrade);

            if (double.IsNaN(percentGrade))
                return "";

            if (percentGrade >= 95 && percentGrade <= 100)
                return "A+";

            if (percentGrade >= 90 && percentGrade < 95)
                return "A";

            if (percentGrade >= 85 && percentGrade < 90)
                return "A-";

            if (percentGrade >= 80 && percentGrade < 85)
                return "B+";

            if (percentGrade >= 75 && percentGrade < 80)
                return "B";

            if (percentGrade >= 70 && percentGrade < 75)
                return "B-";

            if (percentGrade >= 65 && percentGrade < 70)
                return "C+";

            if (percentGrade >= 60 && percentGrade < 65)
                return "C";

            return "C-";
        }
    }
}

﻿using AutoMapper;
using ECO.Entities;
using EmailCampaignOptimizer.Models;

namespace EmailCampaignOptimizer.Core
{
    public static class MappingConfiguration
    {
        public static void Configure()
        {
                
        }
    }

    public static class OptimizeMappingController
    {
        public static void Configure()
        {
            Mapper.CreateMap<EmailValidationResult, RulesModel>()
                .ForMember(x => x.ReturnMessage,
                           s => s.MapFrom(r => r.Valid ? r.ReportRule.SuccessMsg : r.ReportRule.FailMsg))
                .ForMember(x => x.Success, s => s.MapFrom(r => r.Valid));

        }
    }
}
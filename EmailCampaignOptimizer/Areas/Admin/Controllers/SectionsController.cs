﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ECO.Entities;
using ECO.Services;
using EmailCampaignOptimizer.Areas.Admin.Models.Sections;

namespace EmailCampaignOptimizer.Areas.Admin.Controllers
{
    public class SectionsController : Controller
    {
        private readonly IEmailReportService _emailReportService;

        public SectionsController(IEmailReportService emailReportService)
        {
            _emailReportService = emailReportService;
        }

        public ActionResult Index()
        {
            var sections = _emailReportService.GetSections();
            var sectionsModel = new ManageSectionsViewModel { Sections = new List<ManageSectionViewModel>() };

            foreach (var reportSection in sections)
                sectionsModel.Sections.Add(new ManageSectionViewModel { Percentage = reportSection.Percentage, Id = reportSection.Id, Name = reportSection.Title });

            return View(sectionsModel);
        }

        [HttpPost]
        public ActionResult UpdateSectionName(long id, string name)
        {
            var section = _emailReportService.GetSections().FirstOrDefault(x => x.Id == id);
            if (section == null)
                throw new Exception("Section not found.");

            section.Title = name;
            _emailReportService.SaveSection(section);

            return Json("Section updated successfully.");
        }

        public ActionResult UpdateSectionPercentage(long id, int percentage)
        {
            
            var section = _emailReportService.GetSections().FirstOrDefault(x => x.Id == id);
            if (section == null)
                throw new Exception("Section not found.");

            section.Percentage = percentage;

            _emailReportService.SaveSection(section);

            return Json("Section updated successfully.");
        }

        [HttpPost]
        public ActionResult UpdateTopicName(long id, string name)
        {
            var topic = _emailReportService.GetTopicById(id);
            if (topic == null)
                throw new Exception("Topic not found.");

            topic.Title = name;
            _emailReportService.SaveTopic(topic);

            return Json("Topic updated successfully.");
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateTopicMoreInformation(long id, string moreInformation)
        {
            var topic = _emailReportService.GetTopicById(id);
            if (topic == null)
                throw new Exception("Topic not found.");

            topic.MoreInfoLink = moreInformation;
            _emailReportService.SaveTopic(topic);

            return Json("Topic updated successfully.");
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateTopicExamples(long id, string examples)
        {
            var topic = _emailReportService.GetTopicById(id);
            if (topic == null)
                throw new Exception("Topic not found.");

            topic.ExamplesLink = examples;
            _emailReportService.SaveTopic(topic);

            return Json("Topic updated successfully.");
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateTopicAdditionalResources(long id, string additionalResources)
        {
            var topic = _emailReportService.GetTopicById(id);
            if (topic == null)
                throw new Exception("Topic not found.");

            topic.AdditionalResources = additionalResources;
            _emailReportService.SaveTopic(topic);

            return Json("Topic updated successfully.");
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateRuleSuccessMessage(long id, string successMessage)
        {
            var rule = _emailReportService.GetRuleById(id);
            if (rule == null)
                throw new Exception("Rule not found.");

            rule.SuccessMsg = successMessage;
            _emailReportService.SaveRule(rule);

            return Json("Rule updated successfully.");
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateRuleFailMessage(long id, string failMessage)
        {
            var rule = _emailReportService.GetRuleById(id);
            if (rule == null)
                throw new Exception("Rule not found.");

            rule.FailMsg = failMessage;
            _emailReportService.SaveRule(rule);

            return Json("Rule updated successfully.");
        }

        [HttpPost]
        public ActionResult UpdateRuleDictionary(long id, IList<string> listItem)
        {
            var rule = _emailReportService.GetRuleById(id);
            if (rule == null)
                throw new Exception("Rule not found.");

            rule.Dictionary.Clear();

            if (listItem != null)
                foreach (var s in listItem)
                    rule.Dictionary.Add(new ReportRuleDictionary { Rule = rule, Value = s });

            _emailReportService.SaveRule(rule);

            return Json("Rule updated successfully.");
        }

        [HttpPost]
        public ActionResult UpdateRuleStatus(long id, bool enabled)
        {
            var rule = _emailReportService.GetRuleById(id);
            if (rule == null)
                throw new Exception("Rule not found.");

            rule.Enabled = true;
            _emailReportService.SaveRule(rule);

            return Json("Rule Updated Successfully.");
        }

        [HttpPost]
        public ActionResult UpdateRuleWeight(long id, int weight)
        {
            var rule = _emailReportService.GetRuleById(id);
            if (rule == null)
                throw new Exception("Rule not found.");

            rule.Weight = weight;

            _emailReportService.SaveRule(rule);

            return Json("Rule udpated successfully.");
        }

        public ActionResult Section(long id)
        {
            var section = _emailReportService.GetSections().FirstOrDefault(x => x.Id == id);
            if (section == null)
                throw new Exception("Section not found.");

            var model = new ManageSectionViewModel { Id = section.Id, Name = section.Title, Topics = new List<ManageTopicViewModel>() };

            foreach (var topic in section.Topics.OrderBy(x => x.Title))
                model.Topics.Add(new ManageTopicViewModel
                                     {
                                         Id = topic.Id,
                                         Name = topic.Title,
                                         MoreInformation = topic.MoreInfoLink,
                                         Examples = topic.ExamplesLink,
                                         AdditionalResources = topic.AdditionalResources
                                     });

            return View(model);

        }

        public ActionResult Topic(long id)
        {
            var topic = _emailReportService.GetTopicById(id);
            if (topic == null)
                throw new Exception("Topic not found.");

            var rules = topic.ReportRules;

            var topicModel = new ManageTopicViewModel
                                 {
                                     Id = topic.Id,
                                     Name = topic.Title,
                                     SectionId = topic.ReportSection.Id,
                                     Rules = rules.Select(x => new ManageRuleViewModel
                                                                   {
                                                                       Id = x.Id,
                                                                       Name = x.Name,
                                                                       FailMessage = x.FailMsg,
                                                                       SuccessMessage = x.SuccessMsg,
                                                                       Weight = x.Weight,
                                                                       Dictionarys = x.Dictionary.Select(y => new DictionaryModel { Id = y.Id, Value = y.Value }).ToList(),
                                                                       Enabled = x.Enabled
                                                                   }).ToList()
                                 };

            return View(topicModel);
        }
    }
}

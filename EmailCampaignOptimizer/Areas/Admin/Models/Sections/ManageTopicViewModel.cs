﻿using System.Collections.Generic;

namespace EmailCampaignOptimizer.Areas.Admin.Models.Sections
{
    public class ManageTopicViewModel
    {
        public string MoreInformation { get; set; }
        public long Id { get; set; }
        public long SectionId { get; set; }
        public string Name { get; set; }
        public string Examples { get; set; }
        public string AdditionalResources { get; set; }
        public IList<ManageRuleViewModel> Rules { get; set; }
    }
}

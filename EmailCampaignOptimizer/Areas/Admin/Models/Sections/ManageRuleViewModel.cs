﻿using System.Collections.Generic;

namespace EmailCampaignOptimizer.Areas.Admin.Models.Sections
{
    public class ManageRuleViewModel
    {
        public string Name { get; set; }
        public long Id { get; set; }
        public string SuccessMessage { get; set; }
        public string FailMessage { get; set; }
        public IList<DictionaryModel> Dictionarys { get; set; }
        public int Weight { get; set; }
        public bool Enabled { get; set; }
    }
}
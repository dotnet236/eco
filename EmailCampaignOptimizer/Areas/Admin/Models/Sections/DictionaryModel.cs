﻿namespace EmailCampaignOptimizer.Areas.Admin.Models.Sections
{
    public class DictionaryModel
    {
        public long Id { get; set; }
        public string Value { get; set; }
    }
}
﻿using System.Collections.Generic;

namespace EmailCampaignOptimizer.Areas.Admin.Models.Sections
{
    public class ManageSectionViewModel
    {
        public string Name { get; set; }
        public long Id { get; set; }
        public IList<ManageTopicViewModel> Topics { get; set; }
        public int Percentage { get; set; }
    }
}
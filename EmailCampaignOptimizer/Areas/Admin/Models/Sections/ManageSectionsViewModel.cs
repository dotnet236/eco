﻿using System.Collections.Generic;

namespace EmailCampaignOptimizer.Areas.Admin.Models.Sections
{
    public class ManageSectionsViewModel
    {
        public IList<ManageSectionViewModel> Sections { get; set; }
    }
}
﻿using System;
using System.Diagnostics;
using System.Web.Mvc;
using System.Web.Routing;

namespace EmailCampaignOptimizer
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            
            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
            Bootstrapper.Initialize();
        }

        protected  void Application_Error(object sender, EventArgs e)
        {
            var objErr = Server.GetLastError().GetBaseException();
            var source = new EventSourceCreationData("ECO.Web", "Application");
            if (!EventLog.SourceExists(source.Source, source.MachineName))
                EventLog.CreateEventSource(source);

            EventLog.WriteEntry(source.Source, GetInnerMostException(objErr).Message);
        }

        private Exception GetInnerMostException(Exception exception)
        {
            return exception.InnerException == null ? exception : GetInnerMostException(exception.InnerException);
        }
    }
}
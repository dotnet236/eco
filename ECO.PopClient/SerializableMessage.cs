﻿using Limilabs.Mail;

namespace ECO.PopClient
{
    public class SerializableMessage
    {
        public string Subject { get; set; }
        public IMimeDataReadOnlyCollection Attachments { get; set; }
        public string Body { get; set; }
    }
}
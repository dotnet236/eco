﻿using System;
using System.Net;
using System.Net.Mail;

namespace ECO.PopClient
{
    public class ViewReportMessage : MailMessage
    {
        public new MailAddress From = new MailAddress(Configuration.FromAddress, "Email Campaign Optimizer");

        private static SmtpClient _smtpClient;
        public static SmtpClient SmtpClient
        {
            get
            {
                if (_smtpClient == null)
                {
                    _smtpClient = new SmtpClient(Configuration.SmtpServer)
                               {
                                   Credentials = new NetworkCredential(Configuration.FromAddress, Configuration.Password, Configuration.DomainName),
                               };
                }

                return _smtpClient;
            }
        }

        public ViewReportMessage(string reportId, string to) : base(Configuration.FromAddress, to)
        {
            
            var link = string.Format("http://www.{0}/optimize/getReport?reportId={1}", Configuration.DomainName, reportId);
            
            var body = "<html><head>";
body = "<meta content='text/html; charset=utf-8' http-equiv='Content-Type'>";
    body += "<title>Email Campaign Optimizer</title>";
body += "</head>";
body += "<body marginwidth='0' marginheight='0' style='background-color:#2b2b2b;' bottommargin='0' rightmargin='0' topmargin='0' leftmargin='0'>";
body += "<div align='center' style='background-color:#2b2b2b;'>";
body += "<table width='630' cellspacing='0' cellpadding='0' border='0' bgcolor='#2b2b2b' align='center'>";
body += "<tbody>";
            body += "<tr>";
    body += "<td align='center'>";
    body += "<img width='630' height='115' style='display: block;' alt='Email Campaign Optimizer' src='http://img.constantcontact.com/letters/images/1101093164665/eco-em-hdr.jpg'>";
    body += "</td>";
body += "</tr>";
body += "<tr>";
    body += "<td valign='top'>";
    body += "<table width='630' cellspacing='0' cellpadding='0' border='0'>";
    body += "<tbody>";
            body += "<tr>";
        body += "<td width='21' valign='top' rowspan='3'>";
        body += "<img width='21' height='312' style='display: block;' src='http://img.constantcontact.com/letters/images/1101093164665/eco-em-lcol.gif'>";
        body += "</td>";
        body += "<td width='605'>";
        body += "<img width='586' height='62' style='display: block;' alt='Your email has been scored!' src='http://img.constantcontact.com/letters/images/1101093164665/eco-em-title.jpg'>";
        body += "</td>";
        body += "<td width='10' valign='top' rowspan='3'>";
        body += "<img width='23' height='312' style='display: block;' src='http://img.constantcontact.com/letters/images/1101093164665/eco-em-rcol.gif'>";
        body += "</td>";
    body += "</tr>";
    body += "<tr>";
    	body += "<td valign='top'>";
        body += "<table width='586' cellspacing='0' cellpadding='0' border='0' style='background-color:#FFFFFF;'>";
        body += "<tbody><tr>";
        	body += "<td width='100%' valign='top' style=' color:#2b2b2b; font-family:Arial, Helvetica, sans-serif; font-size:12px; padding:0 0 5px 25px;'>";
			body += "<p><strong><a style='color:#0c8390;' href='" + link + "'>Review your report</a></strong> to see how you can improve your email in just a few easy steps.";
body += "</p>";
			body += "<p>";
            body += "<a href='" + link + "'><img width='142' height='37' border='0' alt='Get your report' src='http://img.constantcontact.com/letters/images/1101093164665/eco-em-rptbtn.gif'>";
            body += "</a></p>";
            body += "</td>";
            body += "<td width='263' align='right'>";
            body += "<img width='263' height='156' src='http://img.constantcontact.com/letters/images/1101093164665/eco-em-titleb.jpg'>";
            body += "</td>";
        body += "</tr>";
        body += "</tbody>";
        body += "</table>";
        body += "</td>";
    body += "</tr>";
    body += "<tr>";
        body += "<td valign='bottom' align='center'>";
        body += "<img width='586' height='94' border='0' style='display: block;' src='http://img.constantcontact.com/letters/images/1101093164665/eco-em-ftr.jpg'>";
        body += "</td>";
    body += "</tr>";
    body += "</tbody></table>";
    body += "</td>";
body += "</tr>";
body += "<tr>";
    body += "<td>";
    body += "<table width='630' cellspacing='0' cellpadding='0' border='0'>";
    body += "<tbody>";
            body += "<tr>";
        body += "<td>";
        body += "<a href='http://www.constantcontact.com'><img border='0' width='307' height='73' style='display: block;' alt='Constant Contact' src='http://img.constantcontact.com/mktg/marketing/campaigns/11-2228/ctct_logo.gif'>";
        body += "</td>";
        body += "<td>";
        body += "<img width='323' height='73' style='display: block;' alt='powered by Knowhow' src='http://img.constantcontact.com/mktg/marketing/campaigns/11-2228/knowhow_logo.gif'>";
        body += "</td>";
    body += "</tr>";
    body += "</tbody>";
    body += "</table>";
    body += "</td>";
body += "</tr>";
body += "</tbody>";
body += "</table>";
body += "</div>";


body += "</body>";
body += "</html>";

            IsBodyHtml = true;
            Subject = "Your Report is Ready";
            Body = body;
        }

        public void Send()
        {
            SmtpClient.Send(this);
        }
    }
}
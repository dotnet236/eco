﻿using Limilabs.Mail;

namespace ECO.PopClient
{
    public class ECOMail
    {
        public IMail Mail { get; set; }
        public string Uid { get; set; }
    }
}
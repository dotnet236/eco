﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess; 
using System.Timers;
using ECO.Entities;
using ECO.Repositories.Base;
using ECO.Services;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Conventions.Helpers;
using Limilabs.Mail;
using Newtonsoft.Json;
using NHibernate;

namespace ECO.PopClient
{
    public partial class PopClientService : ServiceBase
    {
        private static Timer _timer;

        private static ISessionFactory SessionFactory
        {
            get
            {
                return Fluently.Configure()
                    .Database(MySQLConfiguration.Standard.ConnectionString(x =>
                                                                                         {
                                                                                             x.Server("localhost");
                                                                                             x.Database("ECO");
                                                                                             x.Username("root");
                                                                                             x.Password("root");
                                                                                         }))
                 .Mappings(m => m.FluentMappings.AddFromAssemblyOf<EmailReportMap>()
                     .Conventions.Add(Table.Is(x => x.TableName.Replace("eco.", "")))
                     .Conventions.Add(PrimaryKey.Name.Is(x =>
                     {
                         var fqn = x.EntityType.ToString();
                         var lastPeriondIndex = fqn.LastIndexOf('.') + 1;
                         if (lastPeriondIndex > fqn.Length) lastPeriondIndex = 0;
                         return String.Format("{0}Id", fqn.Substring(lastPeriondIndex));
                     })))
                    .BuildSessionFactory();
            }
        }
        public static IEmailReportService EmailReportService
        {
            get
            {
                var repo = new BaseRepository(SessionFactory.OpenSession());
                return new EmailReportService(repo);
            }
        }

        public PopClientService()
        {
            InitializeComponent();
        }

        public void Start()
        {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            _timer = new Timer();
            _timer.Elapsed += Execute;
            _timer.Interval = 15000;
            _timer.Enabled = true;
        }

        protected static void Execute(object source, ElapsedEventArgs e)
        {
            ParseMessages();
        }

        private static void ParseMessages()
        {
            _timer.Enabled = false;
            var source = new EventSourceCreationData("ECO.Web", "Application");
            var eventLog = new EventLog { Source = source.Source };

            try
            {
                using (var emailAccount = new EmailAccount("inbox@emailcampaignoptimizer.com", "salink8335"))
                {
                    eventLog.WriteEntry("Retrieving messages...");
                    //string fileName = Limilabs.Mail.Licensing.LicenseHelper.GetLicensePath();
                    //Limilabs.Mail.Licensing.LicenseStatus status = Limilabs.Mail.Licensing.LicenseHelper.GetLicenseStatus();
                    //eventLog.WriteEntry("Mail.dll license: " + fileName + ", " + status.ToString() + ".");

                    var messages = emailAccount.Messages;
                    eventLog.WriteEntry(messages.Count + " new messages found.");
                    foreach (var message in messages)
                        try { 
                            var report = ParseMessage(message.Mail);
                            new ViewReportMessage(report.ReportId, report.UserEmailAddress).Send();
                        }
                        finally { emailAccount.DeleteMessage(message); }
                }
            }
            catch (Exception ex)
            {
                eventLog.WriteEntry(GetInnerMostException(ex).Message);
            }
            finally
            {
                _timer.Enabled = true;
            }
        }

        private static Exception GetInnerMostException(Exception exception)
        {
            return exception.InnerException == null ? exception : GetInnerMostException(exception.InnerException);
        }

        private static EmailReport ParseMessage(IMail message)
        {
            var serializer = new JsonSerializer();
            using (var textWriter = new StringWriter())
            {
                var report = GetEmailReport(message);
                if (report == null)
                    throw new Exception("   Invalid address" + message.To.FirstOrDefault());

                serializer.Serialize(textWriter, new SerializableMessage { Body = message.HtmlData.Text, Subject = message.Subject });
                report.Email = textWriter.ToString();
                report.Status = EmailStatus.Received;
                Console.WriteLine("    Saving email report");
                return EmailReportService.SaveEmailReport(report);
            }
        }

        private static EmailReport GetEmailReport(IMail message)
        {
            var to = message.To.FirstOrDefault();
            if (to == null)
                return null;
            var reportId = to.ToString().Split(new[] { "Address='" }, StringSplitOptions.RemoveEmptyEntries)[1];
            reportId = reportId.Split(new[] { "@" }, StringSplitOptions.RemoveEmptyEntries)[0];
            Console.WriteLine("    Retrieving email report " + reportId);
            return EmailReportService.GetReportById(reportId);
        }

        protected override void OnStop()
        {
            ViewReportMessage.SmtpClient.Dispose();
            if (!SessionFactory.IsClosed)
                SessionFactory.Close();
            _timer.Dispose();
        }
    }
}

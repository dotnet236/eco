﻿using System.ServiceProcess;

namespace ECO.PopClient
{
    static class Program
    {
        static void Main()
        {
            var servicesToRun = new ServiceBase[] { new PopClientService() };
            ServiceBase.Run(servicesToRun);
        }
    }
}

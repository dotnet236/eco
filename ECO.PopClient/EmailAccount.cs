﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Limilabs.Client;
using Limilabs.Mail;

namespace ECO.PopClient
{
    public class EmailAccount : IDisposable
    {
        private readonly string _username;
        private readonly string _password;
        private const string PopServer = "emailcampaignoptimizer.com";

        private Limilabs.Client.POP3.Pop3 _pop3Client;
        public Limilabs.Client.POP3.Pop3 Pop3Client
        {
            get
            {
                if (_pop3Client == null)
                {
                    _pop3Client = new Limilabs.Client.POP3.Pop3();
                    _pop3Client.Connect(PopServer);
                }
                return _pop3Client;
            }
        }

        private IList<ECOMail> _messages;
        public IList<ECOMail> Messages
        {
            get
            {
                if (_messages == null)
                {
                    _messages = new List<ECOMail>();

                    var messageUids = Pop3Client.GetAll();
                    var mailBuilder = new MailBuilder();

                    _messages = messageUids.Select(messageUid => new ECOMail
                                                                     {
                                                                         Mail = mailBuilder.CreateFromEml(Pop3Client.GetMessageByUID(messageUid)),
                                                                         Uid = messageUid
                                                                     }).ToList();
                }

                return _messages;
            }
        }

        public EmailAccount(string username, string password)
        {
            _username = username;
            _password = password;

            Login();
        }

        private void Login()
        {
            Console.WriteLine("Logging into POP server...");
            Pop3Client.Login(_username, _password);
        }

        private void LogOut()
        {
            Pop3Client.Close();
            Console.WriteLine("Logging out of POP server...");
        }

        public void ClearCache()
        {
            _messages = null;
        }

        public void Dispose()
        {
            LogOut();
            _pop3Client.Dispose();
        }

        public void DeleteMessage(ECOMail message)
        {
            Pop3Client.DeleteMessageByUID(message.Uid);
        }
    }
}
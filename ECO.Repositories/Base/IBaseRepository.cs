﻿using System.Collections.Generic;
using FluentNHibernate.Data;
using NHibernate;
using NHibernate.Criterion;

namespace ECO.Repositories.Base
{
    public interface IBaseRepository
    {
        ITransaction BeginTransaction();
        T SaveOrUpdate<T>(T entity, ITransaction trans = null) where T : Entity;
        T Insert<T>(T entity, ITransaction trans = null) where T : Entity;
        IList<T> SaveOrUpdate<T>(IList<T> entities, ITransaction trans = null) where T : Entity;
        T Get<T>(int id) where T : Entity;
        T Get<T>(DetachedCriteria usersSearchCriteria) where T : Entity;
        IEnumerable<T> List<T>(DetachedCriteria searchCriteria) where T : Entity;
        T Get<T>(long id) where T : Entity;
        void Delete(Entity entity, ITransaction trans = null);
        IList<T> List<T>();
    }
}

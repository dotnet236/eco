﻿using System.Collections.Generic;
using FluentNHibernate.Data;
using FluentNHibernate.Mapping;

namespace ECO.Entities
{
    public class ReportSection : Entity
    {
        public virtual string Title { get; set; }
        public virtual IList<ReportTopic> Topics { get; set; }
        public virtual int Percentage { get; set; }
    }

    public class ReportSectionMap : ClassMap<ReportSection>
    {
        public ReportSectionMap()
        {
            Table("ReportSections");
            Id(x => x.Id);
            Map(x => x.Title);
            Map(x => x.Percentage);
            HasMany(x => x.Topics).KeyColumn("ReportSectionId").ForeignKeyConstraintName("ReportSectionId");
        }
    }
}

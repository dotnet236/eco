﻿using System.Collections.Generic;
using System.IO;
using FluentNHibernate.Data;
using FluentNHibernate.Mapping;
using Newtonsoft.Json;

namespace ECO.Entities
{
    public class EmailReport : Entity
    {
        public virtual string ReportId { get; set; }
        public virtual string UserEmailAddress { get; set; }
        public virtual EmailStatus Status { get; set; }
        public virtual string Industry { get; set; }
        public virtual string CreationApplication { get; set; }
        public virtual string Email { get; set; }

        public virtual SerializableMessage GetEmailMessage()
        {
            var serializer = new JsonSerializer();
            return serializer.Deserialize(new StringReader(Email), typeof (SerializableMessage)) as SerializableMessage;
        } 
    }

    public sealed class EmailReportMap : ClassMap<EmailReport>
    {
        public EmailReportMap()
        {
            Table("EmailReport");
            Id(x => x.Id);
            Map(x => x.ReportId);
            Map(x => x.UserEmailAddress);
            Map(x => x.Status);
            Map(x => x.Industry);
            Map(x => x.CreationApplication);
            Map(x => x.Email);

        }
    }
}

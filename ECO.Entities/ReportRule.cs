﻿using System.Collections.Generic;
using FluentNHibernate.Data;
using FluentNHibernate.Mapping;

namespace ECO.Entities
{
    public class ReportRule : Entity
    {
        public ReportRule()
        {
            Weight = 10;
            Dictionary = new List<ReportRuleDictionary>();
        }
        public virtual ReportTopic ReportTopic { get; set; }
        public virtual string Name { get; set; }
        public virtual string SuccessMsg { get; set; }
        public virtual string FailMsg { get; set; }
        public virtual int Weight { get; set; }
        public virtual IList<ReportRuleDictionary> Dictionary { get; set; }
        public virtual bool Enabled { get; set; }
    }

    public class ReportRulesMap : ClassMap<ReportRule>
    {
        public ReportRulesMap()
        {
            Table("ReportRules");
            Id(x => x.Id);
            Map(x => x.Name);
            Map(x => x.SuccessMsg);
            Map(x => x.FailMsg);
            Map(x => x.Weight);
            Map(x => x.Enabled);

            References(x => x.ReportTopic).Column("ReportTopicId");
            HasMany(x => x.Dictionary).KeyColumn("ReportRuleId").Inverse().Cascade.AllDeleteOrphan();
        }
    }
}
﻿namespace ECO.Entities
{
    public class EmailValidationResult
    {
        public ReportRule ReportRule { get; set; }
        public bool Valid { get; set; }
    }
}
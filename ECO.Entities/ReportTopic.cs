﻿using System.Collections.Generic;
using FluentNHibernate.Data;
using FluentNHibernate.Mapping;

namespace ECO.Entities
{
    public class ReportTopic: Entity
    {
        public virtual ReportSection ReportSection { get; set; }
        public virtual string Title { get; set; }
        public virtual string MoreInfoLink { get; set; }
        public virtual string ExamplesLink { get; set; }
        public virtual string AdditionalResources { get; set; }
        public virtual IList<ReportRule> ReportRules { get; set; }
    }

    public class ReportTopicMap : ClassMap<ReportTopic>
    {
        public ReportTopicMap()
        {
            Table("ReportTopics");
            Id(x => x.Id);
            Map(x => x.Title);
            Map(x => x.MoreInfoLink);
            Map(x => x.ExamplesLink);
            Map(x => x.AdditionalResources);

            References(x => x.ReportSection).Column("ReportSectionId");
            HasMany(x => x.ReportRules).KeyColumn("ReportTopicId");
        }
    }
}
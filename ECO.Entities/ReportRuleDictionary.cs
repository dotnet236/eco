﻿using FluentNHibernate.Data;
using FluentNHibernate.Mapping;

namespace ECO.Entities
{
    public class ReportRuleDictionary : Entity
    {
        public virtual ReportRule Rule { get; set; }
        public virtual string Value { get; set; }
    }

    public class EmailReportDictionaryMap : ClassMap<ReportRuleDictionary>
    {
        public EmailReportDictionaryMap()
        {
            Table("ReportRuleDictionary");

            Id(x => x.Id).GeneratedBy.Increment();

            Map(x => x.Value);

            References(x => x.Rule).Column("ReportRuleId").ForeignKey("ReportRuleId");
        }
        
    }
}
﻿using System.Collections.Generic;

namespace ECO.Entities
{
    public class EmailValidationSummary
    {
        public EmailReport Report { get; set; }
        public IList<EmailValidationResult> Results { get; set; }
    }
}

﻿namespace ECO.Entities
{
    public enum EmailStatus
    {
        Received,
        Waiting
    }
}

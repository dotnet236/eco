﻿namespace ECO.Entities
{
    public class SerializableMessage
    {
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}